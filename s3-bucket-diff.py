from __future__ import print_function
import boto3
import sys
import argparse

# Input is two s3 buckets
# assumes user has authed and can run aws cli commands
# creates s3 boto objects
# creates dictionary of contents
# compares contents of dictionary
# if same, adds entry to same dictionary
# if different, adds entry to different dictionary
# reports differences
# usege - command bucket1 bucket2 -o file

# Create an s3 client, will be passed along to the functions called below
s3_client = boto3.client('s3')

def parse_opts():
    """Help messages (-h, --help)."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('-b1', '--bucket1', type=str,
                        help='First bucket to compare.')
    parser.add_argument('-b2', '--bucket2', type=str,
                        help='Second bucket to compare.')                    
    return parser.parse_args()

def check_aws_creds():
    #returns true if able to make boto calls
    client = boto3.client('sts')
    connected = client.get_caller_identity()
    if connected['Account'] != "":
        return True
    else:
        return False

def check_inputs(bucket1, bucket2, s3_client_handler):
    # returns true if s3 buckets exist and are reachable
    if s3_client_handler.head_bucket(Bucket=bucket1) and s3_client_handler.head_bucket(Bucket=bucket2): # this checks to determine if bucket exists and you can access it
        return True
    else:
        return False

def load_bucket_objects(bucket, s3_client_handler):
    # returns a dictionary with the contents of the buckets
    result = s3_client_handler.list_objects_v2(Bucket=bucket)
    return result

def compare_bucket_contents(bucket1, bucket2):
    # Takes key from bucket1, searches if it is in bucket2
    # if it is in bucket2 compares etag and size
    # if those are the same, adds to same_files_dict
    # if they are not, adds to diff_files_dict
    # The dictionaries returned by the s3 call have a list of dicts
    # called contents that contains the list of files.
    master_list = []
    files_list_1 = []
    files_list_2 = []
    for i in range(len(bucket1['Contents'])):
        result = bucket1['Contents'][i]['ETag']
        files_list_1.append(result)
    for i in range(len(bucket2['Contents'])):
        result = bucket2['Contents'][i]['ETag']
        files_list_2.append(result)
    master_list = list(set(files_list_1) | set(files_list_2)) # Files in both buckets
    diff_a = list(set(files_list_1) - set(files_list_2)) # Files only in first bucket
    diff_b = list(set(files_list_2) - set(files_list_1)) # Files only in second bucket
    both_ab = list(set(files_list_1) & set(files_list_2)) # Files in either A or B or both

    for i in range(len(bucket1['Contents'])):
        if bucket1['Contents'][i]['ETag'] in diff_a:
            print(bucket1['Contents'][i]['Key'], " not in ", sys.argv[2])
    for i in range(len(bucket2['Contents'])):
        if bucket2['Contents'][i]['ETag'] in diff_b:
            print(bucket2['Contents'][i]['Key'], " not in ", sys.argv[1])
    for i in range(len(master_list)):
        if bucket1['Contents'][i]['ETag'] in master_list:
            print(bucket1['Contents'][i]['Key']," is the same in both buckets.")


def main():

    args = parse_opts()
    bucket1 = args.bucket1
    bucket2 = args.bucket2
    # Checking the boto connection can be made and the buckets can be reached
    if not check_aws_creds():
        print("Could not find AWS credentials.\n")
        quit()
    if not check_inputs(bucket1, bucket2, s3_client):
        print("Inputs not valid s3 buckets.\n")
        quit()
    # Making two dictionaries to hold the contents of the s3 buckets
    bucket1_contents=load_bucket_objects(bucket1, s3_client)
    bucket2_contents=load_bucket_objects(bucket2, s3_client)
    compare_bucket_contents(bucket1_contents,bucket2_contents)

if __name__ == "__main__":
    main()